from django.shortcuts import render
import requests
from django.http import JsonResponse
from rest_framework import status
from pymongo import MongoClient
import json
import re
from django.utils.safestring import mark_safe


# Create your views here.








def Drug(request):
    return render(request, 'dos.html')


def Drug_Profile(request):
        drug = {}
        client = MongoClient('localhost', 27017)
        # creating database
        db = client['config']
        # creating acollection
        collec = db['drugs']
        generic_names = list(collec.distinct('product_scientific_name'))
        brand_names = list(collec.distinct('product_name_en'))
        if 'drugname' in request.GET:
            drugname = request.GET['drugname']
            url_brand = 'https://api.fda.gov/drug/label.json?search=openfda.brand_name:' + drugname
            url_generic = 'https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + drugname
            response_brand = requests.get(url_brand)
            response_generic = requests.get(url_generic)
            message = 'Drug NOT Found'
            r = requests.get('https://api.fda.gov/drug/label.json?search=openfda.brand_name:' + drugname)
            if drugname in generic_names:
                drugname = drugname.upper()
                search_request = requests.get(
                    'https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + drugname)
                cursor = list(collec.aggregate([{"$unwind": "$product_scientific_name"},
                                                {"$match":
                                                     {'product_scientific_name': drugname}},
                                                {"$group": {"_id": "$product_scientific_name",
                                                            "product_name_en": {
                                                                "$addToSet": "$product_name_en"}}}]))
                try:
                    message1 = "feature not found"

                    # # Convert dict to string
                    # data = json.dumps(drugs)
                    #
                    # mes = data.split(',')
                    # lst = '\n'.join(mes)
                    # print(lst)
                    #
                    # message = mes
                    # context = {"message": message,
                    #            "interact": interact,
                    #            "lst": lst,
                    #            }
                    try:
                        interaction = search_request.json()['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '"]}', '(7.2)"]}', ' [ see Warnings and Precautions ( 5 .6)',
                                  '[ see Warnings and Precautions ( 5 . 6 )]',
                                  '[ see Warnings and Precautions ( 5 . 11 )]',
                                  '[ see Warnings and Precautions ( 5 . 2 )]']

                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    try:
                        clinical_pharmacology = search_request.json()['results'][0]["clinical_pharmacology"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = message1

                    try:
                        sideeffects = search_request.json()['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = message1

                    try:
                        warning = search_request.json()['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = search_request.json()['results'][0]["warnings"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        precaution = search_request.json()['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        pregnancy = r.json()['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        stop_use = r.json()['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        description = search_request.json()['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = search_request.json()['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = search_request.json()['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        dosage = search_request.json()['results'][0]["dosage_forms_and_strengths"]
                    except:
                        dosage = search_request.json()['results'][0]['dosage_and_administration'],

                    try:
                        generic_name = search_request.json()['results'][0]['openfda']['generic_name'],
                    except:
                        generic_name = drugname

                    drug = {

                        "name": drugname,
                        "generic_name": generic_name,
                        'dosage': dosage,
                        "description": description,
                        "Brand_name": cursor[0]['product_name_en'],
                        "purpose": purpose,
                        "precaution": precaution,
                        'usage': search_request.json()['results'][0]["indications_and_usage"],
                        'contraindications_to_use': contraindications_to_use,
                        "stop_use": stop_use,
                        "pregnancy": pregnancy,
                        'sideeffects': sideeffects,
                        'clinical_pharmacology': clinical_pharmacology,
                        'warning': warn,
                        'interaction': interact,
                        'message': message1

                    }

                    return render(request, 'drug.html', {'drug': drug})
                except(TypeError, KeyError):
                    return JsonResponse({"message": "error500"}, status=status.HTTP_200_OK, safe=False)

            elif drugname in brand_names:
                drugname = drugname.upper()
                cursor = collec.find_one({'product_name_en': drugname})
                generics = cursor['product_scientific_name']
                y = cursor["sell_clause"]
                z = cursor["product_scientific_name"]
                print(generics)
                generics = generics.split('+')
                # message1 = "feature not found"

                # for i in generics:
                if z != "":

                    r = requests.get('https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + z)
                else:
                    r = requests.get('https://api.fda.gov/drug/label.json?search=openfda.brand_name:' + drugname)

                try:
                    message1 = "feature not found"

                    # myquery = {"product_name_en": drugname}
                    #
                    # mydoc = collec.find(myquery)
                    #
                    # for x in mydoc:
                    #     print(x)
                    # y = x["buy_price"]
                    # z = x["product_scientific_name"]
                    # print(y)
                    # search_request = requests.get('https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + z)

                    try:
                        generic_name = r.json()['results'][0]['openfda']['generic_name']
                        # generic_name = generi_name and z
                    except:
                        generic_name = z
                    try:
                        interaction = r.json()['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '"]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    # Splitting on UpperCase using re
                    # res_list = [s for s in re.split("([A-Z][^A-Z]*)", ini_str) if s]
                    try:
                        clinical_pharmacology = r.json()['results'][0]["pharmacokinetics"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = "feature not found"

                    try:
                        sideeffects = r.json()['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = "feature not found"

                    try:
                        pregnancy = r.json()['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        warning = r.json()['results'][0]["warnings"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = r.json()['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        precaution = r.json()['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        stop_use = r.json()['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        description = r.json()['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = r.json()['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = r.json()['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    drug = {"name": drugname,
                            "price": y,
                            "generic_name": generic_name,
                            "Brand_name": drugname,
                            "precaution": precaution,
                            "description": description,
                            'stop_use': stop_use,
                            'purpose': purpose,
                            'dosage': r.json()['results'][0]['dosage_and_administration'],
                            'usage': r.json()['results'][0]['indications_and_usage'],
                            'contraindications_to_use': contraindications_to_use,
                            'sideeffects': sideeffects,
                            'pregnancy': pregnancy,
                            'clinical_pharmacology': clinical_pharmacology,
                            'warning': warn,
                            'interaction': interact,
                            'message': message1

                            }

                    return render(request, 'drug.html', {'drug': drug})

                except:
                    return JsonResponse({"message": "error egy brand"}, status=status.HTTP_200_OK, safe=False)





            elif response_generic.status_code == 200:
                try:
                    r = response_generic.json()
                    try:
                        Brand_name = r['results'][0]['openfda']['brand_name'],
                    except:
                        Brand_name = ""

                    try:
                        interaction = r['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '."]}', '(7.2)"]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    try:
                        clinical_pharmacology = r['results'][0]["clinical_pharmacology"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = "feature not found"
                    try:
                        sideeffects = r['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = "feature not found"

                    try:
                        warning = r['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = r['results'][0]["warnings"],
                        # Convert dict to strings
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        pregnancy = r['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        dosage = r['results'][0]["dosage_forms_and_strengths"]
                    except:
                        dosage = r['results'][0]['dosage_and_administration'],

                    try:
                        precaution = r['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        stop_use = r['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        description = r['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = r['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = r['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        generic_name = r['results'][0]['openfda']['generic_name'],
                    except:
                        generic_name = ""

                    message1 = "feature not found"
                    drug = {

                        'name': drugname,
                        'generic_name': generic_name,
                        'Brand_name': Brand_name,
                        'dosage': dosage,
                        'description': description,
                        'purpose': purpose,
                        'precaution': precaution,
                        'stop_use': stop_use,
                        'usage': r['results'][0]["indications_and_usage"],
                        'contraindications_to_use': contraindications_to_use,
                        'sideeffects': sideeffects,
                        "pregnancy": pregnancy,
                        'clinical_pharmacology': clinical_pharmacology,
                        'warning': warn,
                        'interaction': interact,
                        "message": message1,

                    }

                    return render(request, 'drug.html', {'drug': drug})
                except(TypeError, KeyError):
                    return JsonResponse({"message": "error"}, status=status.HTTP_200_OK, safe=False)

            elif response_brand.status_code == 200:
                try:
                    r = response_brand.json()
                    # warning = r['results'][0]["warnings"],
                    # # Convert dict to string
                    # warnings = json.dumps({"warning": warning})

                    # remove = ['{"warning": [["', '"]]}', '(i.e']
                    # for value in remove:
                    #     my_string = warnings.replace(value, '')
                    # warn = my_string.split('.')
                    # warn = re.split(r'(?<=[:.])(?<!\d.)\s', warnings)
                    # remove = ['{"warning": [["', '"]]}', '(i.e']
                    # for value in remove:
                    #     my_string = warn.replace(value, '')
                    # warn = my_string

                    try:
                        interaction = r['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '."]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    # Splitting on UpperCase using re
                    # res_list = [s for s in re.split("([A-Z][^A-Z]*)", ini_str) if s]

                    try:
                        clinical_pharmacology = r['results'][0]["pharmacokinetics"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = "feature not found"

                    try:
                        sideeffects = r['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = "feature not found"

                    try:
                        pregnancy = r['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        warning = r['results'][0]["warnings"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = r['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        dosage = r['results'][0]["dosage_forms_and_strengths"]
                    except:
                        dosage = r['results'][0]['dosage_and_administration'],

                    try:
                        description = r['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = r['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = r['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        precaution = r['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        stop_use = r['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        generic_name = r['results'][0]['openfda']['generic_name'],
                    except:
                        generic_name = ""

                    try:
                        brand_name = r['results'][0]['openfda']['brand_name'],
                    except:
                        brand_name = ""

                    message1 = "feature not found"

                    drug = {
                        'name': drugname,
                        'generic_name': generic_name,
                        'Brand_name': brand_name,
                        'dosage': dosage,
                        'purpose': purpose,
                        'description': description,
                        'precaution': precaution,
                        'stop_use': stop_use,
                        'usage': r['results'][0]["indications_and_usage"],
                        'contraindications_to_use': contraindications_to_use,
                        'sideeffects': sideeffects,
                        'pregnancy': pregnancy,
                        'clinical_pharmacology': clinical_pharmacology,
                        'warning': warn,
                        'interaction': interact,
                        "message": message1,

                    }
                    return render(request, 'drug.html', {'drug': drug})

                except(TypeError, KeyError):
                    return JsonResponse({"message": "error"}, status=status.HTTP_200_OK, safe=False)
            else:
                return JsonResponse("Drug NOT Found", status=status.HTTP_200_OK, safe=False)
                # return render(request, 'message.html', {})

        else:
            return render(request, 'index.html')
def professional(request):

        client = MongoClient('localhost', 27017)
        # creating database
        db = client['config']
        # creating acollection
        collec = db['drugs']
        generic_names = list(collec.distinct('product_scientific_name'))
        brand_names = list(collec.distinct('product_name_en'))
        if 'drugname' in request.GET:
            drugname = request.GET['drugname']
            url_brand = 'https://api.fda.gov/drug/label.json?search=openfda.brand_name:' + drugname
            url_generic = 'https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + drugname
            response_brand = requests.get(url_brand)
            response_generic = requests.get(url_generic)
            message = 'Drug NOT Found'
            r = requests.get('https://api.fda.gov/drug/label.json?search=openfda.brand_name:' + drugname)
            if drugname in generic_names:
                drugname = drugname.upper()
                search_request = requests.get(
                    'https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + drugname)
                cursor = list(collec.aggregate([{"$unwind": "$product_scientific_name"},
                                                {"$match":
                                                     {'product_scientific_name': drugname}},
                                                {"$group": {"_id": "$product_scientific_name",
                                                            "product_name_en": {
                                                                "$addToSet": "$product_name_en"}}}]))
                try:
                    message1 = "feature not found"

                    # # Convert dict to string
                    # data = json.dumps(drugs)
                    #
                    # mes = data.split(',')
                    # lst = '\n'.join(mes)
                    # print(lst)
                    #
                    # message = mes
                    # context = {"message": message,
                    #            "interact": interact,
                    #            "lst": lst,
                    #            }
                    try:
                        interaction = search_request.json()['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '"]}', '(7.2)"]}', ' [ see Warnings and Precautions ( 5 .6)',
                                  '[ see Warnings and Precautions ( 5 . 6 )]',
                                  '[ see Warnings and Precautions ( 5 . 11 )]',
                                  '[ see Warnings and Precautions ( 5 . 2 )]']

                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    try:
                        clinical_pharmacology = search_request.json()['results'][0]["clinical_pharmacology"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = message1

                    try:
                        sideeffects = search_request.json()['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = message1

                    try:
                        warning = search_request.json()['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = search_request.json()['results'][0]["warnings"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        precaution = search_request.json()['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        pregnancy = r.json()['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        stop_use = r.json()['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        description = search_request.json()['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = search_request.json()['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = search_request.json()['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        dosage = search_request.json()['results'][0]["dosage_forms_and_strengths"]
                    except:
                        dosage = search_request.json()['results'][0]['dosage_and_administration'],

                    try:
                        generic_name = search_request.json()['results'][0]['openfda']['generic_name'],
                    except:
                        generic_name = drugname

                    try:
                        overdosage = search_request.json()['results'][0]["overdosage"],
                    except:
                        overdosage = "feature not found"

                    try:
                        how_supplied = search_request.json()['results'][0]["how_supplied"],
                    except:
                        how_supplied = "feature not found"


                    try:
                        dosage_and_administration = search_request.json()['results'][0]["dosage_and_administration"]
                    except:
                        dosage_and_administration = "feature not found"

                    try:
                        information_for_patients = search_request.json()['results'][0]["information_for_patients"],
                        information_for_patients = json.dumps({"information_for_patients": information_for_patients})
                        my_string = information_for_patients
                        remove = ['{"information_for_patients": [["', '"]]}',' [ see Warnings and Precautions ( 5.3 ) ]','( 5.1 )',' ( 5.5 )',' ( 5.7 )']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        information_for_patients = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    except:
                        information_for_patients = "feature not found"


                    drug = {

                        "name": drugname,
                        "generic_name": generic_name,
                        'dosage': dosage,
                        "description": description,
                        "Brand_name": cursor[0]['product_name_en'],
                        "purpose": purpose,
                        "precaution": precaution,
                        'usage': search_request.json()['results'][0]["indications_and_usage"],
                        'contraindications_to_use': contraindications_to_use,
                        "stop_use": stop_use,
                        "pregnancy": pregnancy,
                        'sideeffects': sideeffects,
                        'clinical_pharmacology': clinical_pharmacology,
                        'warning': warn,
                        'interaction': interact,
                        'overdosage': overdosage,
                        'how_supplied': how_supplied,
                        'dosage_and_administration': dosage_and_administration,
                        'information_for_patients': information_for_patients,
                        'message': message1

                    }

                    return render(request, 'professional.html', {'drug': drug})
                except(TypeError, KeyError):
                    return JsonResponse({"message": "error500"}, status=status.HTTP_200_OK, safe=False)

            elif drugname in brand_names:
                drugname = drugname.upper()
                cursor = collec.find_one({'product_name_en': drugname})
                generics = cursor['product_scientific_name']
                y = cursor["sell_clause"]
                z = cursor["product_scientific_name"]
                print(generics)
                generics = generics.split('+')
                # message1 = "feature not found"

                # for i in generics:
                if z != "":

                    r = requests.get('https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + z)
                else:
                    r = requests.get('https://api.fda.gov/drug/label.json?search=openfda.brand_name:' + drugname)

                try:
                    message1 = "feature not found"

                    # myquery = {"product_name_en": drugname}
                    #
                    # mydoc = collec.find(myquery)
                    #
                    # for x in mydoc:
                    #     print(x)
                    # y = x["buy_price"]
                    # z = x["product_scientific_name"]
                    # print(y)
                    # search_request = requests.get('https://api.fda.gov/drug/label.json?search=openfda.generic_name:' + z)

                    try:
                        generic_name = r.json()['results'][0]['openfda']['generic_name']
                        # generic_name = generi_name and z
                    except:
                        generic_name = z
                    try:
                        interaction = r.json()['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '"]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    # Splitting on UpperCase using re
                    # res_list = [s for s in re.split("([A-Z][^A-Z]*)", ini_str) if s]
                    try:
                        clinical_pharmacology = r.json()['results'][0]["pharmacokinetics"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = "feature not found"

                    try:
                        sideeffects = r.json()['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = "feature not found"

                    try:
                        pregnancy = r.json()['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        warning = r.json()['results'][0]["warnings"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = r.json()['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        precaution = r.json()['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        stop_use = r.json()['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        description = r.json()['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = r.json()['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = r.json()['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        overdosage = search_request.json()['results'][0]["overdosage"],
                    except:
                        overdosage = "feature not found"

                    try:
                        how_supplied = search_request.json()['results'][0]["how_supplied"],
                    except:
                        how_supplied = "feature not found"

                    try:
                        dosage_and_administration = search_request.json()['results'][0]["dosage_and_administration"]
                    except:
                        dosage_and_administration = "feature not found"

                    try:
                        information_for_patients = search_request.json()['results'][0]["information_for_patients"],
                        information_for_patients = json.dumps({"information_for_patients": information_for_patients})
                        my_string = information_for_patients
                        remove = ['{"information_for_patients": [["', '"]]}',
                                  ' [ see Warnings and Precautions ( 5.3 ) ]', '( 5.1 )', ' ( 5.5 )', ' ( 5.7 )']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        information_for_patients = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    except:
                        information_for_patients = "feature not found"

                    drug = {"name": drugname,
                            "price": y,
                            "generic_name": generic_name,
                            "Brand_name": drugname,
                            "precaution": precaution,
                            "description": description,
                            'stop_use': stop_use,
                            'purpose': purpose,
                            'dosage': r.json()['results'][0]['dosage_and_administration'],
                            'usage': r.json()['results'][0]['indications_and_usage'],
                            'contraindications_to_use': contraindications_to_use,
                            'sideeffects': sideeffects,
                            'pregnancy': pregnancy,
                            'clinical_pharmacology': clinical_pharmacology,
                            'warning': warn,
                            'interaction': interact,
                            'overdosage': overdosage,
                            'how_supplied': how_supplied,
                            'dosage_and_administration': dosage_and_administration,
                            'information_for_patients': information_for_patients,
                            'message': message1

                            }

                    return render(request, 'professional.html', {'drug': drug})

                except:
                    return JsonResponse({"message": "error egy brand"}, status=status.HTTP_200_OK, safe=False)





            elif response_generic.status_code == 200:
                try:
                    r = response_generic.json()
                    try:
                        Brand_name = r['results'][0]['openfda']['brand_name'],
                    except:
                        Brand_name = ""

                    try:
                        interaction = r['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '."]}', '(7.2)"]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    try:
                        clinical_pharmacology = r['results'][0]["clinical_pharmacology"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = "feature not found"
                    try:
                        sideeffects = r['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = "feature not found"

                    try:
                        warning = r['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = r['results'][0]["warnings"],
                        # Convert dict to strings
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        pregnancy = r['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        dosage = r['results'][0]["dosage_forms_and_strengths"]
                    except:
                        dosage = r['results'][0]['dosage_and_administration'],

                    try:
                        precaution = r['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        stop_use = r['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        description = r['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = r['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = r['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        generic_name = r['results'][0]['openfda']['generic_name'],
                    except:
                        generic_name = ""

                    try:
                        overdosage = search_request.json()['results'][0]["overdosage"],
                    except:
                        overdosage = "feature not found"

                    try:
                        how_supplied = search_request.json()['results'][0]["how_supplied"],
                    except:
                        how_supplied = "feature not found"

                    try:
                        dosage_and_administration = search_request.json()['results'][0]["dosage_and_administration"]
                    except:
                        dosage_and_administration = "feature not found"

                    try:
                        information_for_patients = search_request.json()['results'][0]["information_for_patients"],
                        information_for_patients = json.dumps({"information_for_patients": information_for_patients})
                        my_string = information_for_patients
                        remove = ['{"information_for_patients": [["', '"]]}',
                                  ' [ see Warnings and Precautions ( 5.3 ) ]', '( 5.1 )', ' ( 5.5 )', ' ( 5.7 )']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        information_for_patients = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    except:
                        information_for_patients = "feature not found"

                    message1 = "feature not found"
                    drug = {

                        'name': drugname,
                        'generic_name': generic_name,
                        'Brand_name': Brand_name,
                        'dosage': dosage,
                        'description': description,
                        'purpose': purpose,
                        'precaution': precaution,
                        'stop_use': stop_use,
                        'usage': r['results'][0]["indications_and_usage"],
                        'contraindications_to_use': contraindications_to_use,
                        'sideeffects': sideeffects,
                        "pregnancy": pregnancy,
                        'clinical_pharmacology': clinical_pharmacology,
                        'warning': warn,
                        'interaction': interact,
                        'overdosage': overdosage,
                        'how_supplied': how_supplied,
                        'dosage_and_administration': dosage_and_administration,
                        'information_for_patients': information_for_patients,
                        "message": message1,

                    }

                    return render(request, 'professional.html', {'drug': drug})
                except(TypeError, KeyError):
                    return JsonResponse({"message": "error"}, status=status.HTTP_200_OK, safe=False)

            elif response_brand.status_code == 200:
                try:
                    r = response_brand.json()
                    # warning = r['results'][0]["warnings"],
                    # # Convert dict to string
                    # warnings = json.dumps({"warning": warning})

                    # remove = ['{"warning": [["', '"]]}', '(i.e']
                    # for value in remove:
                    #     my_string = warnings.replace(value, '')
                    # warn = my_string.split('.')
                    # warn = re.split(r'(?<=[:.])(?<!\d.)\s', warnings)
                    # remove = ['{"warning": [["', '"]]}', '(i.e']
                    # for value in remove:
                    #     my_string = warn.replace(value, '')
                    # warn = my_string

                    try:
                        interaction = r['results'][0]["drug_interactions"]
                        interact = json.dumps({" interaction": interaction})
                        my_string = interact
                        remove = ['{" interaction": ["', '."]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        interact = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        interact = "feature not found"

                    # Splitting on UpperCase using re
                    # res_list = [s for s in re.split("([A-Z][^A-Z]*)", ini_str) if s]

                    try:
                        clinical_pharmacology = r['results'][0]["pharmacokinetics"],
                        clinical_pharmacology = json.dumps({"clinical_pharmacology": clinical_pharmacology})
                        my_string = clinical_pharmacology
                        remove = ['{"clinical_pharmacology": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        clinical_pharmacology = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        clinical_pharmacology = "feature not found"

                    try:
                        sideeffects = r['results'][0]["adverse_reactions"],
                        sideeffects = json.dumps({"sideeffects": sideeffects})
                        my_string = sideeffects
                        remove = ['{"sideeffects": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        sideeffects = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        sideeffects = "feature not found"

                    try:
                        pregnancy = r['results'][0]["pregnancy"],
                        pregnancy = json.dumps({" pregnancy": pregnancy})
                        my_string = pregnancy
                        remove = ['{" pregnancy": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        pregnancy = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        pregnancy = "feature not found"

                    try:
                        warning = r['results'][0]["warnings"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)
                    except:
                        warning = r['results'][0]["warnings_and_cautions"],
                        # Convert dict to string
                        warnings = json.dumps({"warning": warning})
                        my_string = warnings
                        remove = ['{"warning": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        warn = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    try:
                        dosage = r['results'][0]["dosage_forms_and_strengths"]
                    except:
                        dosage = r['results'][0]['dosage_and_administration'],

                    try:
                        description = r['results'][0]['description'],
                        # Convert dict to string
                        description = json.dumps({"description": description})
                        my_string = description
                        remove = ['{"description": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        description = my_string
                    except:
                        description = "feature not found"

                    try:
                        contraindications_to_use = r['results'][0]["contraindications"],
                    except:
                        contraindications_to_use = "feature not found"

                    try:
                        purpose = r['results'][0]['purpose'],
                    except:
                        purpose = "feature not found"

                    try:
                        precaution = r['results'][0]['precautions'],
                        # Convert dict to string
                        precaution = json.dumps({"precaution": precaution})
                        my_string = precaution
                        remove = ['{"precaution": [["', '"]]}']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        precaution = re.split(r'(?<=[;.])(?<!\d.)\s', my_string)
                    except:
                        precaution = "feature not found"

                    try:
                        stop_use = r['results'][0]["stop_use"],
                    except:
                        stop_use = "feature not found"

                    try:
                        generic_name = r['results'][0]['openfda']['generic_name'],
                    except:
                        generic_name = ""

                    try:
                        brand_name = r['results'][0]['openfda']['brand_name'],
                    except:
                        brand_name = ""

                    try:
                        overdosage = search_request.json()['results'][0]["overdosage"],
                    except:
                        overdosage = "feature not found"

                    try:
                        how_supplied = search_request.json()['results'][0]["how_supplied"],
                    except:
                        how_supplied = "feature not found"

                    try:
                        dosage_and_administration = search_request.json()['results'][0]["dosage_and_administration"]
                    except:
                        dosage_and_administration = "feature not found"

                    try:
                        information_for_patients = search_request.json()['results'][0]["information_for_patients"],
                        information_for_patients = json.dumps({"information_for_patients": information_for_patients})
                        my_string = information_for_patients
                        remove = ['{"information_for_patients": [["', '"]]}',
                                  ' [ see Warnings and Precautions ( 5.3 ) ]', '( 5.1 )', ' ( 5.5 )', ' ( 5.7 )']
                        for value in remove:
                            my_string = my_string.replace(value, '')
                        information_for_patients = re.split(r'(?<=[/.])(?<!\d.)\s', my_string)

                    except:
                        information_for_patients = "feature not found"

                    message1 = "feature not found"

                    drug = {
                        'name': drugname,
                        'generic_name': generic_name,
                        'Brand_name': brand_name,
                        'dosage': dosage,
                        'purpose': purpose,
                        'description': description,
                        'precaution': precaution,
                        'stop_use': stop_use,
                        'usage': r['results'][0]["indications_and_usage"],
                        'contraindications_to_use': contraindications_to_use,
                        'sideeffects': sideeffects,
                        'pregnancy': pregnancy,
                        'clinical_pharmacology': clinical_pharmacology,
                        'warning': warn,
                        'interaction': interact,
                        'overdosage': overdosage,
                        'how_supplied': how_supplied,
                        'dosage_and_administration': dosage_and_administration,
                        'information_for_patients': information_for_patients,
                        "message": message1,

                    }
                    return render(request, 'professional.html', {'drug': drug})

                except(TypeError, KeyError):
                    return JsonResponse({"message": "error"}, status=status.HTTP_200_OK, safe=False)
            else:
                return JsonResponse("Drug NOT Found", status=status.HTTP_200_OK, safe=False)
                # return render(request, 'message.html', {})

        else:
            return render(request, 'index.html')



