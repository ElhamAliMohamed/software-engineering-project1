from django.urls import path
from . import views
from django.conf.urls import url
app_name = 'drugs'

urlpatterns = [

    url('drugprofile/', views.Drug_Profile, name="drug"),
	url('professional/', views.professional, name="professional"),
    url('', views.Drug, name='Drugs'),

]